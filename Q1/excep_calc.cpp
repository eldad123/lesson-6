#include <iostream>
#define ILLEGAL_NUM 8200

/*
function will add number to other
input: two numbers
output: the result
*/
int add(int a, int b) {
	//if one of the parameter is 8200 , throw error
	if (a + b == ILLEGAL_NUM || a == ILLEGAL_NUM || b == ILLEGAL_NUM)
	{
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year");
	}
	return a + b;
}

/*
function will multiply number with other
input: two numbers
output: the result
*/
int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		//if one of the parameter is 8200 , throw error
		if (sum == ILLEGAL_NUM || a == ILLEGAL_NUM || b == ILLEGAL_NUM)
		{
			throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year");
		}
	};

	return sum;
}

/*
function will pow number with other
input: two numbers
output: the result
*/
int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		//if one of the parameter is 8200 , throw error
		if (exponent == ILLEGAL_NUM || a == ILLEGAL_NUM || b == ILLEGAL_NUM)
		{
			throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year");
		}
	};
	return exponent;
}


int main(void) {
	std::cerr << "Calc 1: " << std::endl;
	//Run calculation (without errors)
	try
	{
		std::cout << pow(3, 3) << std::endl;
	}
	//if there was an error, print it
	catch (std::string s)
	{
		std::cerr << s << std::endl;
	}
	std::cerr << "Calc 2: "<< std::endl;

	//Run calculation (with errors)
	try
	{
		std::cout << pow(ILLEGAL_NUM, 3) << std::endl;
	}
	//if there was an error, print it
	catch (std::string s)
	{
		std::cerr << s;
	}
	

}