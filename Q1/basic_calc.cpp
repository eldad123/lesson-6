#include <iostream>
#define ILLEGAL_NUM 8200
#define ERROR_SIGN -1
#define NO_ERROR_SIGN -2

/*
function will add number with other number and put the result in reference var
input: two numbers
output: if there was not an error
*/
int add(int a, int b, int& result) {
	result = a + b;
	if (a == ILLEGAL_NUM || b == ILLEGAL_NUM|| a + b == ILLEGAL_NUM)
	{
		return ERROR_SIGN;
	}
	else
	{
		return NO_ERROR_SIGN;
	}
}

/*
function multiply add number with other number and put the result in reference var
input: two numbers
output: if there was not an error
*/
int  multiply(int a, int b, int& result) {
	int  sum = 0;
	for (int i = 0; i < b; i++) {
		if (a == ILLEGAL_NUM || b == ILLEGAL_NUM || add(sum, a, sum) == ERROR_SIGN || sum == ILLEGAL_NUM)
		{
			return ERROR_SIGN;
		}
	}
	result = sum;
	return NO_ERROR_SIGN;
}

/*
function will pow add number with other number and put the result in reference var
input: two numbers
output: if there was not an error
*/
int  pow(int a, int b, int& result) {
	int ex = 1;
	for (int i = 0; i < b; i++) {
		if (a == ILLEGAL_NUM || b == ILLEGAL_NUM || multiply(ex, a, ex) == ERROR_SIGN || ex == ILLEGAL_NUM)
		{
			return ERROR_SIGN;
		}
	}
	result = ex;
	return NO_ERROR_SIGN;
	
}

/*int main(void) {
	int result1 = 0, result2 = 0; //results of two calculates
	int try1 = pow(7, 3, result1); //first calculate (without error)
	int try2 = pow(ILLEGAL_NUM, 3, result1); //second calculate (without error)
	if (try1 == ERROR_SIGN) //if the first calculate had an error, print error message
	{
		std::cout << "1: This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	else
	{
		std::cout << "result of 7 in 3: " << result1 << std::endl;
	}

	if (try2 == ERROR_SIGN) //if the second calculate had an error, print error message
	{
		std::cout << "2: This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	else
	{
		std::cout << result2 << std::endl;
	}
}*/
