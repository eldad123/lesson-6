#pragma once
#include "shape.h"
#include <math.h>
#define THREE 3
class MathUtils : public Shape
{
public:
	static double CalPentagonArea(double x);
	static double CalHexagonArea(double x);
};
