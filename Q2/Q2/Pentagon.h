#pragma once

#include "shape.h"
#include "MathUtils.h"
#include <iostream>

class Pentagon :public Shape {
public:
	Pentagon(std::string name, std::string col, double mem);
	void draw();
	double CalArea();
	void setMember(const double mem);
	double getMember();

private:
	double member;

};