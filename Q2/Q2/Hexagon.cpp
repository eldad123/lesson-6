#include "Hexagon.h"
#include "shapeException.h"

//functions will draw the hexagon
void Hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Member " << getMember() << std::endl;
}

/*
c'tor
input: hexagon name, cilor and member
output: none
*/
Hexagon::Hexagon(std::string name, std::string col, double mem) :Shape(name, col)
{
	setMember(mem);
}

/*
function will set the member
input: new value for the member
output: none 
*/
void Hexagon::setMember(const double mem)
{
	if (mem < 0)
	{
		throw shapeException();
	}
	this->member = mem;
}


//function will return the member
double Hexagon::getMember()
{
	return this->member;
}

//function will calculate the area of the hexagon
double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(this->member);
}
