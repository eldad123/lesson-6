#include "Pentagon.h"
#include "shapeException.h"

/*
c'tor
input: pentagon name, cilor and member
output: none
*/
Pentagon::Pentagon(std::string name, std::string col, double mem) :Shape(name, col)
{
	setMember(mem);
}

//functions will draw the pentagon
void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Member " << getMember() << std::endl;
}

//function will return the member
double Pentagon::getMember()
{
	return this->member;
}

/*
function will set the member
input: new value for the member
output: none
*/
void Pentagon::setMember(const double mem)
{
	if (mem < 0)
	{
		throw shapeException();
	}
	this->member = mem;
}

//function will calculate the area of the pentagon
double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->member);
}