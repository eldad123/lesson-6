#pragma once

#include "shape.h"
#include "MathUtils.h"
#include <iostream>

class Hexagon :public Shape {
public:
	Hexagon(std::string name, std::string col, double mem); //c'tor
	void draw();
	double CalArea(); 
	void setMember(const double mem); //set the member
	double getMember(); //get the member


private:
	double member;

};