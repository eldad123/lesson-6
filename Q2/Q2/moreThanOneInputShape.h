#pragma once
#include <exception>

class moreThanOneInputShape : public std::exception
{
	//if the user entered more than one char
	virtual const char* what() const
	{
		return "Warning - Don't try to build more than one shape at once!\n";
	}
};
