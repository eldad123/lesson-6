#include "MathUtils.h"
//function will calculate pentagon area
double MathUtils::CalPentagonArea(double x)
{
	return 1.72048 * x * x;
}

//function will calculate hexagon area
double MathUtils::CalHexagonArea(double x)
{
	return (THREE * sqrt(THREE) * x * x) / 2;
}
