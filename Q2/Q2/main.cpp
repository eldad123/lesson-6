#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "Pentagon.h"
#include "Hexagon.h"
#include <string>
#include "shapeException.h"
#include "inputException.h"
#include "moreThanOneInputShape.h"
#define MAX_DEG 90
int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, member = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pen(nam, col, member);
	Hexagon hex(nam, col, member);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrPen = &pen; //add pentagon pointer
	Shape *ptrHex = &hex; //add hexagon pointer


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'P', hexagon  = 'h'; //add hexagon and pentagon
	std::string shapetype; 
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram" << std::endl;
		try
		{
			std::getline(std::cin, shapetype);
			if (shapetype.length() > 1 || shapetype.length() < 0) //check if the user entered more than one char
			{
				throw moreThanOneInputShape();
			}
		}
		catch (std::exception & e)
		{
			printf(e.what());
		}

try
		{

			switch (shapetype[0]) //add shape in first place to get only first char(task 1.3)
			{
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				if (!(std::cin >> col >> nam >> rad)) // or if(cin.fail())
				{
					// user didn't input a number
					std::cin.clear(); // reset failbit
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip bad input
					throw inputException();
				}
				//if the radius is lower than 0, throw shape exception
				if (rad < 0)
				{
					throw shapeException();
				}
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				if (!(std::cin >> nam >> col >> height >> width))
				{
					// user didn't input a number
					std::cin.clear(); // reset failbit
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip bad input
					throw inputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				if (!(std::cin >> nam >> col >> height >> width >> ang >> ang2))
				{
					// user didn't input a number
					std::cin.clear(); // reset failbit
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip bad input
					throw inputException();
				}
				if (ang > MAX_DEG || ang < 0 || ang2 > MAX_DEG || ang2 < 0)
				{
					throw shapeException();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'P':
				std::cout << "enter name, color, and member" << std::endl;
				if (!(std::cin >> nam >> col >> member))
				{
					// user didn't input a number
					std::cin.clear(); // reset failbit
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip bad input
					throw inputException();
				}
				if (member < 0)
				{
					throw shapeException();
				}
				pen.setName(nam);
				pen.setColor(col);
				pen.setMember(member);
				break;
			case 'h':
				std::cout << "enter name, color, and member" << std::endl;
				if (!(std::cin >> nam >> col >> member))
				{
					// user didn't input a number
					std::cin.clear(); // reset failbit
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip bad input
					throw inputException();
				}
				if (member < 0)
				{
					throw shapeException();
				}
				hex.setName(nam);
				hex.setColor(col);
				hex.setMember(member);
				break;


			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (std::exception& e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}