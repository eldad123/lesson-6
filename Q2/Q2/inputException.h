#pragma once
#include <exception>

class inputException : public std::exception
{
	//if the input is not a number
	virtual const char* what() const
	{
		return "That is not a number!\n";
	}
};
